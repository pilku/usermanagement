var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var OrganizationSchema = new Schema({
	name : String,
	_id : String,
	description : String
});


module.exports = mongoose.model('Organization',OrganizationSchema);