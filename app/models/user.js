var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
	name : String,
	_id : String,
	phoneNumber : String,
	orgId : String,
	password : String,
	roleIds : [String]
});


module.exports = mongoose.model('User',UserSchema);