var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RoleSchema = new Schema({
	name : String,
	_id : String,
	description : String,
	permissionsIds : [String]
});

module.exports = mongoose.model('Role',RoleSchema);