var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PermissionSchema = new Schema({
	name : String,
	_id : String,
	description : String
});

module.exports = mongoose.model('Permission',PermissionSchema);