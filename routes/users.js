var express = require('express');

var router = express.Router(); 

//to generate short unique id
var shortId = require('shortid');

//importing user model
var User = require('../app/models/user');

var usersService = require('../services/usersService');

var async = require('async');


/*this is middleware just like java's filter (gets called before servlet)
which is used to intercept the request and 
process it according to the requirement
one of them can be validaion.
*/
router.use(function(req,res,next){

	
	next();
});



router.route("/users")
	.post(function(req,res){

		var user = new User();	
		user.name = req.body.name;
		user.phoneNumber = req.body.phoneNumber;
		user._id = shortId.generate();
		user.password = req.body.password;

		async.parallel([
			function(callback){
				usersService.createUser(user,callback);
			}
			],function(err,results){
			if(err)
				res.status(500).send(err);

				else{
					res.json({user: user,message:'user has been created'});
				}
		});

	});


// var createSuperAdmin = function(){
// 	User.findOne({'name':'SuperAdmin'},function(err,doc){
// 		if(!doc){
// 			var superAdmin = new User();
// 			superAdmin.name = "SuperAdmin";
// 			superAdmin.phoneNumber = "9021567894";
// 			superAdmin._id = shortId.generate();
// 			superAdmin.save(function(){});
// 		}
// 	});
// };

module.exports = router;
// module.exports.createSuperAdmin = createSuperAdmin;	
