var express = require('express');

var router = express.Router();

var Organization = require('../app/models/organization');
var User = require('../app/models/user');

var organizationsService = require('../services/organizationsService');
var usersService = require('../services/usersService');

var shortId = require('shortid');

var async = require('async');

router.use(function(req,res,next){
	next();
});

router.route('/organizations')
	.post(function(req,res){

		var organization = new Organization();
		organization.name = req.body.org.name;
		organization._id = shortId.generate();
		organization.description = req.body.org.description;

		var user = new User();
		user.name = req.body.user.name;
		user.phoneNumber = req.body.user.phoneNumber;
		user.orgId = organization._id;
		user.password = req.body.user.password;
		user._id = shortId.generate();

		async.parallel({
			org : function(callback){
				organizationsService.createOrganization(organization,callback);
			},
			org_user : function(callback){
				usersService.createUser(user,callback);
			}

			},function(err,results){
			if(err){
				//here refactore the code and use async series and
				 //then if error occures at any point then remove previous data that has been inserted to db in this operation

				Organization.findOneAndRemove({_id:organization._id},function(err){});

				res.status(500).send(err);
			}
			else{
				res.json({result: results,message:'organization has been created along with super admin.'});
			}
		});
	})

module.exports = router;