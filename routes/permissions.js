var express = require('express');

var router = express.Router();

//importing permission model
var Permission = require('../app/models/permission');

//to generate short unique id
var shortId = require('shortid');

var async = require('async');

var permissionService = require('../services/permissionsService');

router.use(function(req,res,next){
	if(req.originalUrl === "/usermanagement/permissions"){
		
	}

	
	next();
});


router.route('/permissions')
	.post(function(req,res){

		var permission = new Permission();
		permission.name = req.body.name;
		permission.description = req.body.description;
		permission._id = shortId.generate();

		async.parallel([
				function(callback){
					permissionService.createPermission(permission,callback);
				}
			],function(err,results){
			if(err)
				res.status(500).send(err);
			else{
				res.json({permission: permission,message:'permission has been created'});
			}
		});
	});


module.exports = router;