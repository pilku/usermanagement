var express = require('express');

var router = express.Router(); 

//importing role model
var Role = require('../app/models/role');

//to generate short unique id
var shortId = require('shortid');

var rolesService = require('../services/rolesService');

var permissionService = require('../services/permissionsService');

var async = require('async');

router.use(function(req,res,next){

	
	next();
});

router.route("/roles")
	.post(function(req,res){

		var role = new Role();
		role.name = req.body.name;
		role.description = req.body.description;
		role._id = shortId.generate();

		async.parallel([
				function(callback){
					rolesService.createRole(role,callback);
				}
			],function(err,results){
				if(err)
					res.status(500).send(err);

				else{
					res.json({role: role,message:'role has been created'});
				}
		});

	});

module.exports = router;