var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var users = require('./routes/users');
var roles = require('./routes/roles');
var permissions = require('./routes/permissions');
var organizations = require('./routes/organizations');
var loginService = require('./services/loginService');
var usersService = require('./services/usersService');
var async = require('async');
var session = require('express-session');

app.use(cookieParser());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.use(session({
			secret : "loggedUser",
			cookie : {maxAge : 60000},
			resave: true,
    		saveUninitialized: true
}));

var port = process.env.PORT || 8081;

//MongoDB connection
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/usermanagement');

//app level middlewear

app.use(function(req,res,next){

	// here i will get /usermanagement/organizations
	// now i can decide which request to fwd and which i should not.



	if(req.originalUrl === "/usermanagement/organizations" || req.originalUrl === "/login")
		next();
	else{

		if(req){
			var userId = req.session.userId;
			var orgId = req.session.orgId;

		async.parallel([
				function(callback){
					usersService.checkIfUserExist(userId,orgId,callback);
				}
			],function(err,result){
				if(err){
					res.status(500).send({message : "Please login."});
				}
				else{
					next();
				}
			});
		}
		else{
			res.status(500).send({message : "Please login."});
		}
	}
});

app.use('/usermanagement', users);
app.use('/usermanagement', roles);
app.use('/usermanagement', permissions);
app.use('/usermanagement', organizations);



app.post('/login',function(req,res){
	async.parallel({
			login : function(callback){
				var reqObj = {
					username : req.body.username,
					orgId : req.body.orgId,
					password : req.body.password
				};
				loginService.login(reqObj,callback);
			}

		
		},function(err,results){
		if(err)
			res.status(500).send(err);

		else{
		req.session.userId = results.login._id;
		req.session.orgId = results.login.orgId;
		res.json({message:'you have successfully logged in.'});
		}
	});
});


app.listen(port,function(){
	if(mongoose.connection.readyState === 2){

		// users.createSuperAdmin();
	}
});


console.log('This server is running on port ' + port);