//to generate short unique id
var shortId = require('shortid');

var User = require('../app/models/user');



var createUser = function(user,callback){
	if(user.name && user.phoneNumber && user.password){
			user.save(function(err){
				if(err)
					callback(err,null);

				else{
					callback(null,user);
				}
			});
		}
		else{
			var err = { error: "user name and/or phoneNumber and/or password invalid or empty." };
			callback(err,null);
		}
};


var checkIfUserExist = function(userId,orgId,callback){
	User.count({_id:userId,orgId:orgId},function(err,count){
		if(err)
			callback(err,null);

		if(count > 0)
			callback(null,count);
		else
		{
			var userDoesNotExistError = {error:"User does not exist with id "+userId};
			callback(userDoesNotExistError,null);
		}
	});
};


module.exports.checkIfUserExist = checkIfUserExist;
module.exports.createUser = createUser;