var Permission = require('../app/models/permission');

var async = require('async');

var createPermission = function(permission,callback){
	if(permission.name && permission.description){
			permission.save(function(err){
				if(err)
					callback(err,null);

				else{
					callback(null,permission);
				}
			});
		}
		else{
			var err = { error: "permission name and/or invalid or empty." };

			callback(err,null);
		}
}

var checkIfPermissionExist = function(permissionIds,callback){
	Permission.find({
		_id : {
			$all : permissionIds
		}
	},function(err,results){
		if(err)
			callback(err,null);

		if(!results || results.length === 0){
			var permissionErr = {error : "some of the permission id's not present"};
			callback(permissionErr,null);			
		}else{
			callback(null,results);
		}
	});
}

module.exports.checkIfPermissionExist = checkIfPermissionExist;
module.exports.createPermission = createPermission;