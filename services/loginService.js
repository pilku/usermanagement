var User = require('../app/models/user');

var login = function(req,callback){
	if(req.username){
		User.findOne({name : req.username, orgId : req.orgId,password:req.password},function(err,doc){
			if(err || doc===null) {
					var err = {error : "username is not present."};
					callback(err,null);
				}
			else{
				callback(null,doc);
			}
		});
	}
	else{
		var err = {
			error : "username is invalid."
		};

		callback(err,null);
	}
};

module.exports.login =  login;