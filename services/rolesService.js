
var Role = require('../app/models/role');

var createRole = function(role,callback){
	if(role.name && role.description){
			role.save(function(err){
				if(err)
					callback(err,null);

				else{
					callback(null,role);
				}
			});
		}
		else{
			var err = { error: "role name and/or description invalid or empty." };

			callback(err,null);
		}
}


var checkIfRolesExists = function(roleIds,callback){

Role.find({_id:{
				$all:roleIds
			}},function(err,results){
				if(err){
				callback(err,null);
				}

			if(!results || results.length === 0){
				var roleErr = {error : "some of the role id's not present"};
				callback(roleErr,null);			
				}else{
				callback(null,results);
			}	
			});

}

module.exports.createRole = createRole;
module.exports.checkIfRolesExists = checkIfRolesExists;